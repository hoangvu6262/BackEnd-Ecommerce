"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const URL_const_1 = require("../const/URL.const");
const mongodbConnection = () => {
    mongoose_1.default
        .connect(URL_const_1.URI)
        .then(() => {
        console.log('connection to mongodb successful!');
    })
        .catch((err) => {
        console.log(err);
    });
};
exports.default = mongodbConnection;
//# sourceMappingURL=mongodb.config.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const categoryModel = new mongoose_1.Schema({
    name: { type: String, required: true },
    createAt: { type: Date, defaultValue: Date.now() },
    updateAt: { type: Date, defaultValue: Date.now() },
    billboard: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Billboard',
    },
}, { timestamps: true });
exports.default = (0, mongoose_1.model)('Category', categoryModel);
//# sourceMappingURL=category.model.js.map
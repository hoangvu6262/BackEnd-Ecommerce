"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const billboardModel = new mongoose_1.Schema({
    label: { type: String, required: true },
    imageUrl: { type: String, required: false },
    createAt: { type: Date, defaultValue: Date.now() },
    updateAt: { type: Date, defaultValue: Date.now() },
}, { timestamps: true });
exports.default = (0, mongoose_1.model)('Billboard', billboardModel);
//# sourceMappingURL=billboard.model.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const category_route_1 = __importDefault(require("./category.route"));
const billboard_route_1 = __importDefault(require("./billboard.route"));
const product_route_1 = __importDefault(require("./product.route"));
const router = express_1.default.Router();
exports.default = () => {
    (0, category_route_1.default)(router);
    (0, billboard_route_1.default)(router);
    (0, product_route_1.default)(router);
    return router;
};
//# sourceMappingURL=root.route.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const brand_controller_1 = require("../controllers/brand.controller");
const brandRoute = (router) => {
    router.get('/brand/getall', brand_controller_1.getBrands);
    router.post('/brand', brand_controller_1.addCategory);
    router.get('/brand/:id', brand_controller_1.getBrandByID);
    router.delete('/brand/:id', brand_controller_1.deleteCategoryByID);
    router.put('/brand/:id', brand_controller_1.updateCategoryByID);
};
exports.default = brandRoute;
//# sourceMappingURL=brand.route.js.map
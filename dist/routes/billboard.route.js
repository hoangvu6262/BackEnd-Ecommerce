"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const billboard_controller_1 = require("../controllers/billboard.controller");
const billboardRoute = (router) => {
    router.get('/billboard', billboard_controller_1.addCategory);
    router.post('/billboard', billboard_controller_1.getBillboards);
    router.get('/billboard/:id', billboard_controller_1.getBillboardByID);
    router.delete('/billboard/:id', billboard_controller_1.deleteCategoryByID);
    router.put('/billboard/:id', billboard_controller_1.updateCategoryByID);
};
exports.default = billboardRoute;
//# sourceMappingURL=billboard.route.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const product_controller_1 = require("../controllers/product.controller");
const productRoute = (router) => {
    router.get('/product/:id', product_controller_1.getProductByID);
    router.post('/product', product_controller_1.addNewProduct);
    router.delete('/product/:id', product_controller_1.deleteProductByID);
    router.put('/product/:id', product_controller_1.updateProductByID);
};
exports.default = productRoute;
//# sourceMappingURL=product.route.js.map
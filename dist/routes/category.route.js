"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const category_controller_1 = require("../controllers/category.controller");
exports.default = (router) => {
    router.get('/category', category_controller_1.getCategories);
    router.post('/category', category_controller_1.addCategory);
};
//# sourceMappingURL=category.route.js.map
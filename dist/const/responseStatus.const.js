"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RES_STATUS = void 0;
exports.RES_STATUS = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
};
//# sourceMappingURL=responseStatus.const.js.map
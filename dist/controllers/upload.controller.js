"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.uploadImages = void 0;
const cloudinary_config_1 = require("../config/cloudinary.config");
const fs_1 = __importDefault(require("fs"));
const uploadImages = async (req, res) => {
    try {
        const urls = [];
        const { files } = req.body;
        for (const file of files) {
            const { path } = file;
            const newpath = await (0, cloudinary_config_1.cloudinaryUploadImg)(path);
            urls.push(newpath);
            fs_1.default.unlinkSync(path);
        }
        const images = urls.map((file) => {
            return file;
        });
        res.json(images);
    }
    catch (error) {
        throw new Error(error);
    }
};
exports.uploadImages = uploadImages;
//# sourceMappingURL=upload.controller.js.map
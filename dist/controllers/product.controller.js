"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateProductByID = exports.deleteProductByID = exports.addNewProduct = exports.getProductByID = void 0;
const product_services_1 = require("../services/product.services");
const validate_helper_1 = require("../helpers/validate.helper");
const responseStatus_const_1 = require("../const/responseStatus.const");
const getProductByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const product = await (0, product_services_1.getProduct)(id);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ product });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.getProductByID = getProductByID;
const deleteProductByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const product = await (0, product_services_1.deleteProduct)(id);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ product });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.deleteProductByID = deleteProductByID;
const addNewProduct = async (req, res) => {
    try {
        const body = req.body;
        if (!body) {
            throw new Error('Body data request is undefined. Please check your request.');
        }
        const isExistProd = await (0, product_services_1.getProductByName)(body.name);
        if (isExistProd) {
            throw new Error(`Billbard ${body.name} already exists, Please change your billboard's name.`);
        }
        const newProd = await (0, product_services_1.creteNewProduct)(body);
        res.status(responseStatus_const_1.RES_STATUS.CREATED).json(newProd);
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.addNewProduct = addNewProduct;
const updateProductByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const product = await (0, product_services_1.updateProduct)(id, req.body);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ product });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.updateProductByID = updateProductByID;
//# sourceMappingURL=product.controller.js.map
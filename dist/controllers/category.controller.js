"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateCategoryByID = exports.addCategory = exports.deleteCategoryByID = exports.getCategoryID = exports.getCategories = void 0;
const responseStatus_const_1 = require("../const/responseStatus.const");
const category_services_1 = require("../services/category.services");
const validate_helper_1 = require("../helpers/validate.helper");
const getCategories = async (_req, res) => {
    try {
        const categories = await (0, category_services_1.getAllCategories)();
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ categories });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.getCategories = getCategories;
const getCategoryID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const category = await (0, category_services_1.getCategoryById)(id);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ category });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.getCategoryID = getCategoryID;
const deleteCategoryByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const category = await (0, category_services_1.deleteCategoryById)(id);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ category });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.deleteCategoryByID = deleteCategoryByID;
const addCategory = async (req, res) => {
    try {
        const body = req.body;
        if (!body) {
            throw new Error('Body data request is undefined. Please check your request.');
        }
        const isCategory = await (0, category_services_1.getCategoriesByName)(body.name);
        if (isCategory) {
            throw new Error(`Category ${body.name} already exists, Please change your category's name.`);
        }
        const newCate = await (0, category_services_1.createCategory)({
            id: body.id,
            name: body.name,
            billboard: body.billboard,
        });
        res.status(responseStatus_const_1.RES_STATUS.CREATED).json(newCate);
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.addCategory = addCategory;
const updateCategoryByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const category = await (0, category_services_1.updateCategory)(id, req.body);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ category });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.updateCategoryByID = updateCategoryByID;
//# sourceMappingURL=category.controller.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addCategory = exports.updateCategoryByID = exports.deleteCategoryByID = exports.getBrandByID = exports.getBrands = void 0;
const responseStatus_const_1 = require("src/const/responseStatus.const");
const brand_services_1 = require("../services/brand.services");
const validate_helper_1 = require("../helpers/validate.helper");
const getBrands = async (_req, res) => {
    try {
        const brand = await (0, brand_services_1.getAllBrands)();
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ brand });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.getBrands = getBrands;
const getBrandByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const brand = await (0, brand_services_1.getBrand)(id);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ brand });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.getBrandByID = getBrandByID;
const deleteCategoryByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const brand = await (0, brand_services_1.deleteBrand)(id);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ brand });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.deleteCategoryByID = deleteCategoryByID;
const addCategory = async (req, res) => {
    try {
        const body = req.body;
        if (!body) {
            throw new Error('Body data request is undefined. Please check your request.');
        }
        const isExistBrand = await (0, brand_services_1.getBrandByName)(body.name);
        if (isExistBrand) {
            throw new Error(`Brand ${body.name} already exists, Please change your billboard's name.`);
        }
        const newBrand = await (0, brand_services_1.createBrand)(body);
        res.status(responseStatus_const_1.RES_STATUS.CREATED).json(newBrand);
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.addCategory = addCategory;
const updateCategoryByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const brand = await (0, brand_services_1.updateBrand)(id, req.body);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ brand });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.updateCategoryByID = updateCategoryByID;
//# sourceMappingURL=brand.controller.js.map
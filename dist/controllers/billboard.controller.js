"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addCategory = exports.updateCategoryByID = exports.deleteCategoryByID = exports.getBillboardByID = exports.getBillboards = void 0;
const responseStatus_const_1 = require("../const/responseStatus.const");
const billboard_services_1 = require("../services/billboard.services");
const validate_helper_1 = require("../helpers/validate.helper");
const getBillboards = async (_req, res) => {
    try {
        const billboards = await (0, billboard_services_1.getAllBillboards)();
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ billboards });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.getBillboards = getBillboards;
const getBillboardByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const billboard = await (0, billboard_services_1.getBillboard)(id);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ billboard });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.getBillboardByID = getBillboardByID;
const deleteCategoryByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const billboard = await (0, billboard_services_1.deleteBillboard)(id);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ billboard });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.deleteCategoryByID = deleteCategoryByID;
const addCategory = async (req, res) => {
    try {
        const body = req.body;
        if (!body) {
            throw new Error('Body data request is undefined. Please check your request.');
        }
        const isBillboard = await (0, billboard_services_1.getBillboardByName)(body.name);
        if (isBillboard) {
            throw new Error(`Billbard ${body.name} already exists, Please change your billboard's name.`);
        }
        const newBillboard = await (0, billboard_services_1.createBillboard)(body);
        res.status(responseStatus_const_1.RES_STATUS.CREATED).json(newBillboard);
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.addCategory = addCategory;
const updateCategoryByID = async (req, res) => {
    const { id } = req.params;
    (0, validate_helper_1.validateMongoID)(id);
    try {
        const billboard = await (0, billboard_services_1.updateBillboard)(id, req.body);
        res.status(responseStatus_const_1.RES_STATUS.OK).json({ billboard });
    }
    catch (err) {
        throw new Error(err);
    }
};
exports.updateCategoryByID = updateCategoryByID;
//# sourceMappingURL=billboard.controller.js.map
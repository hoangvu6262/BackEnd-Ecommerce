"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createBillboard = exports.deleteBillboard = exports.updateBillboard = exports.getBillboardByName = exports.getBillboard = exports.getAllBillboards = void 0;
const billboard_model_1 = __importDefault(require("../models/billboard.model"));
const getAllBillboards = () => {
    return billboard_model_1.default.find();
};
exports.getAllBillboards = getAllBillboards;
const getBillboard = (id) => {
    return billboard_model_1.default.findById(id);
};
exports.getBillboard = getBillboard;
const getBillboardByName = (name) => {
    return billboard_model_1.default.findOne({ name });
};
exports.getBillboardByName = getBillboardByName;
const updateBillboard = (id, value) => {
    return billboard_model_1.default.findByIdAndUpdate(id, value, { new: true });
};
exports.updateBillboard = updateBillboard;
const deleteBillboard = (id) => {
    return billboard_model_1.default.findByIdAndDelete(id);
};
exports.deleteBillboard = deleteBillboard;
const createBillboard = (value) => {
    return new billboard_model_1.default(value).save().then((billboard) => billboard.toObject());
};
exports.createBillboard = createBillboard;
//# sourceMappingURL=billboard.services.js.map
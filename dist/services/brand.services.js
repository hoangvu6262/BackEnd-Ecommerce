"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createBrand = exports.deleteBrand = exports.updateBrand = exports.getBrandByName = exports.getBrand = exports.getAllBrands = void 0;
const brand_model_1 = __importDefault(require("../models/brand.model"));
const getAllBrands = () => {
    return brand_model_1.default.find();
};
exports.getAllBrands = getAllBrands;
const getBrand = (id) => {
    return brand_model_1.default.findById(id);
};
exports.getBrand = getBrand;
const getBrandByName = (name) => {
    return brand_model_1.default.findOne({ name });
};
exports.getBrandByName = getBrandByName;
const updateBrand = (id, value) => {
    return brand_model_1.default.findByIdAndUpdate(id, value, { new: true });
};
exports.updateBrand = updateBrand;
const deleteBrand = (id) => {
    return brand_model_1.default.findByIdAndDelete(id);
};
exports.deleteBrand = deleteBrand;
const createBrand = async (value) => {
    return brand_model_1.default.create(value);
};
exports.createBrand = createBrand;
//# sourceMappingURL=brand.services.js.map
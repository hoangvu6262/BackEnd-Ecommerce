"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateProduct = exports.creteNewProduct = exports.deleteProduct = exports.getProductByName = exports.getProduct = void 0;
const product_model_1 = __importDefault(require("../models/product.model"));
const getProduct = (id) => {
    return product_model_1.default.findById(id);
};
exports.getProduct = getProduct;
const getProductByName = (name) => {
    return product_model_1.default.findOne({ name });
};
exports.getProductByName = getProductByName;
const deleteProduct = (id) => {
    return product_model_1.default.findByIdAndDelete(id);
};
exports.deleteProduct = deleteProduct;
const creteNewProduct = (data) => {
    return product_model_1.default.create(data);
};
exports.creteNewProduct = creteNewProduct;
const updateProduct = (id, data) => {
    return product_model_1.default.findOneAndUpdate({ id }, data, { new: true });
};
exports.updateProduct = updateProduct;
//# sourceMappingURL=product.services.js.map
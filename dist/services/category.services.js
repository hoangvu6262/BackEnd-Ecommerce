"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateCategory = exports.createCategory = exports.deleteCategoryById = exports.getCategoriesByName = exports.getCategoryById = exports.getAllCategories = void 0;
const category_model_1 = __importDefault(require("../models/category.model"));
const getAllCategories = () => {
    return category_model_1.default.find();
};
exports.getAllCategories = getAllCategories;
const getCategoryById = (id) => {
    return category_model_1.default.findById(id);
};
exports.getCategoryById = getCategoryById;
const getCategoriesByName = (name) => {
    return category_model_1.default.findOne({ name: name });
};
exports.getCategoriesByName = getCategoriesByName;
const deleteCategoryById = (id) => {
    return category_model_1.default.findByIdAndDelete(id);
};
exports.deleteCategoryById = deleteCategoryById;
const createCategory = async (values) => {
    return new category_model_1.default(values).save().then((category) => category.toObject());
};
exports.createCategory = createCategory;
const updateCategory = async (id, values) => {
    return category_model_1.default.findByIdAndUpdate(id, values, { new: true });
};
exports.updateCategory = updateCategory;
//# sourceMappingURL=category.services.js.map
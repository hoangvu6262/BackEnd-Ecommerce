import { Router } from 'express'
import { getCategories, addCategory } from '../controllers/category.controller'

export default (router: Router) => {
    router.get('/category', getCategories)
    router.post('/category', addCategory)
}

// categoryRoutes.get('/menu', getCategories)
// categoryRoutes.post('/menu', addCategory)

// export default categoryRoutes

import express from 'express'
import category from './category.route'
import billboardRoute from './billboard.route'
import productRoute from './product.route'

const router = express.Router()

export default (): express.Router => {
    category(router)
    billboardRoute(router)
    productRoute(router)
    //   users(router);

    return router
}

import { Router } from 'express'
import {
    getBrands,
    getBrandByID,
    deleteCategoryByID,
    updateCategoryByID,
    addCategory,
} from '../controllers/brand.controller'

const brandRoute = (router: Router) => {
    router.get('/brand/getall', getBrands)
    router.post('/brand', addCategory)
    router.get('/brand/:id', getBrandByID)
    router.delete('/brand/:id', deleteCategoryByID)
    router.put('/brand/:id', updateCategoryByID)
}

export default brandRoute

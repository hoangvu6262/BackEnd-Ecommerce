import { Router } from 'express'
import {
    getBillboards,
    getBillboardByID,
    deleteCategoryByID,
    updateCategoryByID,
    addCategory,
} from '../controllers/billboard.controller'

const billboardRoute = (router: Router) => {
    router.get('/billboard', addCategory)
    router.post('/billboard', getBillboards)
    router.get('/billboard/:id', getBillboardByID)
    router.delete('/billboard/:id', deleteCategoryByID)
    router.put('/billboard/:id', updateCategoryByID)
}

export default billboardRoute

import { Router } from 'express'
import {
    getProductByID,
    addNewProduct,
    deleteProductByID,
    updateProductByID,
} from '../controllers/product.controller'

const productRoute = (router: Router) => {
    // router.get("/product/getall", )
    router.get('/product/:id', getProductByID)
    router.post('/product', addNewProduct)
    router.delete('/product/:id', deleteProductByID)
    router.put('/product/:id', updateProductByID)
}

export default productRoute

import mongoose from 'mongoose'

const validateMongoID = (id: string) => {
    if (!mongoose.Types.ObjectId.isValid(id))
        throw new Error('This id is not valid or not Found')
}

export { validateMongoID }

import { model, Schema } from 'mongoose'
import { IOrder } from '../const/commonType.const'

const orderModel: Schema = new Schema(
    {
        products: [
            {
                product: {
                    type: Schema.Types.ObjectId,
                    ref: 'Product',
                },
                count: Number,
                color: String,
            },
        ],
        paymentIntent: {},
        orderStatus: {
            type: String,
            default: 'Not Processed',
            enum: [
                'Not Processed',
                'Cash on Delivery',
                'Processing',
                'Dispatched',
                'Cancelled',
                'Delivered',
            ],
        },
        orderby: {
            type: Schema.Types.ObjectId,
            ref: 'User',
        },
    },
    {
        timestamps: true,
    }
)

export default model<IOrder>('Order', orderModel)

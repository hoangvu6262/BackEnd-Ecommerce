import { ICategory } from '../const/commonType.const'
import { model, Schema } from 'mongoose'

const categoryModel: Schema = new Schema(
    {
        name: { type: String, required: true },
        createAt: { type: Date, defaultValue: Date.now() },
        updateAt: { type: Date, defaultValue: Date.now() },
        billboard: {
            type: Schema.Types.ObjectId,
            ref: 'Billboard',
        },
    },
    { timestamps: true }
)

export default model<ICategory>('Category', categoryModel)

import { model, Schema } from 'mongoose'
import { ICart } from 'src/const/commonType.const'

const cartModel: Schema = new Schema(
    {
        products: [
            {
                product: {
                    type: Schema.Types.ObjectId,
                    ref: 'Product',
                },
                count: Number,
                color: String,
                price: Number,
            },
        ],
        cartTotal: Number,
        totalAfterDiscount: Number,
        orderby: {
            type: Schema.Types.ObjectId,
            ref: 'User',
        },
    },
    {
        timestamps: true,
    }
)

export default model<ICart>('Cart', cartModel)

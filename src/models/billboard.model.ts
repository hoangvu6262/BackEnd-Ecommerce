import { IBillboard } from '../const/commonType.const'
import { model, Schema } from 'mongoose'

const billboardModel: Schema = new Schema(
    {
        label: { type: String, required: true },
        imageUrl: { type: String, required: false },
        createAt: { type: Date, defaultValue: Date.now() },
        updateAt: { type: Date, defaultValue: Date.now() },
    },
    { timestamps: true }
)

export default model<IBillboard>('Billboard', billboardModel)

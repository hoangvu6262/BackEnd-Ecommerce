import { model, Schema } from 'mongoose'
import { IBrand } from '../const/commonType.const'

const brandModel: Schema = new Schema(
    {
        title: {
            type: String,
            required: true,
            unique: true,
            index: true,
        },
    },
    {
        timestamps: true,
    }
)

export default model<IBrand>('Brand', brandModel)

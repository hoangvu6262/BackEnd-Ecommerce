import { model, Schema } from 'mongoose' // Erase if already required
import { IProduct } from '../const/commonType.const'

// Declare the Schema of the Mongo model
var productSchema: Schema = new Schema(
    {
        title: {
            type: String,
            required: true,
            trim: true,
        },
        slug: {
            type: String,
            required: true,
            unique: true,
            lowercase: true,
        },
        description: {
            type: String,
            required: true,
        },
        price: {
            type: Number,
            required: true,
        },
        category: {
            type: Schema.Types.ObjectId,
            ref: 'Category',
        },
        brand: {
            type: Schema.Types.ObjectId,
            ref: 'Brand',
        },
        quantity: {
            type: Number,
            required: true,
        },
        sold: {
            type: Number,
            default: 0,
        },
        images: [
            {
                public_id: String,
                url: String,
            },
        ],
        color: [String],
        tags: String,
        // ratings: [
        //     {
        //         star: Number,
        //         comment: String,
        //         postedby: { type: Schema.Types.ObjectId, ref: 'User' },
        //     },
        // ],
        // totalrating: {
        //     type: String,
        //     default: 0,
        // },
    },
    { timestamps: true }
)

//Export the model
export default model<IProduct>('Product', productSchema)

import { v2 as cloudinary } from 'cloudinary'
import dotenv from 'dotenv'

dotenv.config()

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
})

const cloudinaryUploadImg = (fileToUploads: string) => {
    return new Promise((resolve) => {
        cloudinary.uploader.upload(fileToUploads, (result: any) => {
            resolve({
                url: result.secure_url,
                asset_id: result.asset_id,
                public_id: result.public_id,
            })
        })
    })
}

export { cloudinaryUploadImg }

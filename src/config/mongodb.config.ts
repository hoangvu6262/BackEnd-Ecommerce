import mongoose from 'mongoose'
import { URI } from '../const/URL.const'

const mongodbConnection = () => {
    mongoose
        .connect(URI)
        .then(() => {
            console.log('connection to mongodb successful!')
        })
        .catch((err) => {
            console.log(err)
        })
}

export default mongodbConnection

import Brand from '../models/brand.model'

const getAllBrands = () => {
    return Brand.find()
}

const getBrand = (id: string) => {
    return Brand.findById(id)
}

const getBrandByName = (name: string) => {
    return Brand.findOne({ name })
}

const updateBrand = (id: string, value: Record<string, any>) => {
    return Brand.findByIdAndUpdate(id, value, { new: true })
}

const deleteBrand = (id: string) => {
    return Brand.findByIdAndDelete(id)
}

const createBrand = async (value: Record<string, any>) => {
    return Brand.create(value)
}

export {
    getAllBrands,
    getBrand,
    getBrandByName,
    updateBrand,
    deleteBrand,
    createBrand,
}

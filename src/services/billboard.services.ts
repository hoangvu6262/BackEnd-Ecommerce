import Billboard from '../models/billboard.model'

const getAllBillboards = () => {
    return Billboard.find()
}

const getBillboard = (id: string) => {
    return Billboard.findById(id)
}

const getBillboardByName = (name: string) => {
    return Billboard.findOne({ name })
}

const updateBillboard = (id: string, value: Record<string, any>) => {
    return Billboard.findByIdAndUpdate(id, value, { new: true })
}

const deleteBillboard = (id: string) => {
    return Billboard.findByIdAndDelete(id)
}

const createBillboard = (value: Record<string, any>) => {
    return new Billboard(value).save().then((billboard) => billboard.toObject())
}

export {
    getAllBillboards,
    getBillboard,
    getBillboardByName,
    updateBillboard,
    deleteBillboard,
    createBillboard,
}

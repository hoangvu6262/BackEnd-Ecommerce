import Category from '../models/category.model'

const getAllCategories = () => {
    return Category.find()
}

const getCategoryById = (id: string) => {
    return Category.findById(id)
}

const getCategoriesByName = (name: string) => {
    return Category.findOne({ name: name })
}

const deleteCategoryById = (id: string) => {
    return Category.findByIdAndDelete(id)
}

const createCategory = async (values: Record<string, any>) => {
    return new Category(values).save().then((category) => category.toObject())
}

const updateCategory = async (id: string, values: Record<string, any>) => {
    return Category.findByIdAndUpdate(id, values, { new: true })
}

export {
    getAllCategories,
    getCategoryById,
    getCategoriesByName,
    deleteCategoryById,
    createCategory,
    updateCategory,
}

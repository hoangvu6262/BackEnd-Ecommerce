import Product from '../models/product.model'

const getProduct = (id: string) => {
    return Product.findById(id)
}

const getProductByName = (name: string) => {
    return Product.findOne({ name })
}

const deleteProduct = (id: string) => {
    return Product.findByIdAndDelete(id)
}

const creteNewProduct = (data: Record<string, any>) => {
    return Product.create(data)
}

const updateProduct = (id: string, data: Record<string, any>) => {
    return Product.findOneAndUpdate({ id }, data, { new: true })
}

export {
    getProduct,
    getProductByName,
    deleteProduct,
    creteNewProduct,
    updateProduct,
}

import { Response, Request } from 'express'
import { IBillboard } from '../const/commonType.const'
import { RES_STATUS } from '../const/responseStatus.const'
import {
    getAllBillboards,
    getBillboard,
    getBillboardByName,
    updateBillboard,
    deleteBillboard,
    createBillboard,
} from '../services/billboard.services'
import { validateMongoID } from '../helpers/validate.helper'

const getBillboards = async (_req: Request, res: Response) => {
    try {
        const billboards: IBillboard[] = await getAllBillboards()

        res.status(RES_STATUS.OK).json({ billboards })
    } catch (err) {
        throw new Error(err)
    }
}

const getBillboardByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)

    try {
        const billboard: IBillboard | null = await getBillboard(id)
        res.status(RES_STATUS.OK).json({ billboard })
    } catch (err) {
        throw new Error(err)
    }
}

const deleteCategoryByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const billboard: IBillboard | null = await deleteBillboard(id)
        res.status(RES_STATUS.OK).json({ billboard })
    } catch (err) {
        throw new Error(err)
    }
}

const addCategory = async (req: Request, res: Response) => {
    try {
        const body = req.body
        if (!body) {
            throw new Error(
                'Body data request is undefined. Please check your request.'
            )
        }

        const isBillboard: IBillboard | null = await getBillboardByName(
            body.name
        )
        if (isBillboard) {
            throw new Error(
                `Billbard ${body.name} already exists, Please change your billboard's name.`
            )
        }

        const newBillboard: IBillboard = await createBillboard(body)

        res.status(RES_STATUS.CREATED).json(newBillboard)
    } catch (err) {
        throw new Error(err)
    }
}

const updateCategoryByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const billboard: IBillboard | null = await updateBillboard(id, req.body)
        res.status(RES_STATUS.OK).json({ billboard })
    } catch (err) {
        throw new Error(err)
    }
}

export {
    getBillboards,
    getBillboardByID,
    deleteCategoryByID,
    updateCategoryByID,
    addCategory,
}

import { Request, Response } from 'express'
import { cloudinaryUploadImg } from '../config/cloudinary.config'
import fs from 'fs'

const uploadImages = async (req: Request, res: Response) => {
    try {
        // const uploader = (path: string) => cloudinaryUploadImg(path)
        const urls = []
        const { files } = req.body
        for (const file of files) {
            const { path } = file
            const newpath = await cloudinaryUploadImg(path)
            urls.push(newpath)
            fs.unlinkSync(path)
        }
        const images = urls.map((file) => {
            return file
        })
        res.json(images)
    } catch (error) {
        throw new Error(error)
    }
}

export { uploadImages }

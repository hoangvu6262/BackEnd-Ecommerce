import { Request, Response } from 'express'
import { IProduct } from '../const/commonType.const'
import {
    getProduct,
    getProductByName,
    deleteProduct,
    creteNewProduct,
    updateProduct,
} from '../services/product.services'
import { validateMongoID } from '../helpers/validate.helper'
import { RES_STATUS } from '../const/responseStatus.const'

// const getAllProducts = async (_req: Request, res: Response) => {
//     try {
//         const billboards: IProduct[] = await getAllBillboards()

//         res.status(RES_STATUS.OK).json({ billboards })
//     } catch (err) {
//         throw new Error(err)
//     }
// }

const getProductByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)

    try {
        const product: IProduct | null = await getProduct(id)
        res.status(RES_STATUS.OK).json({ product })
    } catch (err) {
        throw new Error(err)
    }
}

const deleteProductByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const product: IProduct | null = await deleteProduct(id)
        res.status(RES_STATUS.OK).json({ product })
    } catch (err) {
        throw new Error(err)
    }
}

const addNewProduct = async (req: Request, res: Response) => {
    try {
        const body = req.body
        if (!body) {
            throw new Error(
                'Body data request is undefined. Please check your request.'
            )
        }

        const isExistProd: IProduct | null = await getProductByName(body.name)
        if (isExistProd) {
            throw new Error(
                `Billbard ${body.name} already exists, Please change your billboard's name.`
            )
        }

        const newProd: IProduct = await creteNewProduct(body)

        res.status(RES_STATUS.CREATED).json(newProd)
    } catch (err) {
        throw new Error(err)
    }
}

const updateProductByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const product: IProduct | null = await updateProduct(id, req.body)
        res.status(RES_STATUS.OK).json({ product })
    } catch (err) {
        throw new Error(err)
    }
}

export { getProductByID, addNewProduct, deleteProductByID, updateProductByID }

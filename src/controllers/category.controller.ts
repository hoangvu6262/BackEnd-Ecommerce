import { Response, Request } from 'express'
import { ICategory } from '../const/commonType.const'
import { RES_STATUS } from '../const/responseStatus.const'
import {
    getAllCategories,
    getCategoryById,
    getCategoriesByName,
    deleteCategoryById,
    createCategory,
    updateCategory,
} from '../services/category.services'
import { validateMongoID } from '../helpers/validate.helper'

export const getCategories = async (_req: Request, res: Response) => {
    try {
        const categories: ICategory[] = await getAllCategories()
        res.status(RES_STATUS.OK).json({ categories })
    } catch (err) {
        throw new Error(err)
    }
}

export const getCategoryID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const category: ICategory | null = await getCategoryById(id)
        res.status(RES_STATUS.OK).json({ category })
    } catch (err) {
        throw new Error(err)
    }
}

export const deleteCategoryByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const category: ICategory | null = await deleteCategoryById(id)
        res.status(RES_STATUS.OK).json({ category })
    } catch (err) {
        throw new Error(err)
    }
}

export const addCategory = async (req: Request, res: Response) => {
    try {
        const body = req.body
        if (!body) {
            throw new Error(
                'Body data request is undefined. Please check your request.'
            )
        }

        const isCategory: ICategory | null = await getCategoriesByName(
            body.name
        )
        if (isCategory) {
            throw new Error(
                `Category ${body.name} already exists, Please change your category's name.`
            )
        }

        const newCate: ICategory = await createCategory({
            id: body.id,
            name: body.name,
            billboard: body.billboard,
        })

        res.status(RES_STATUS.CREATED).json(newCate)
    } catch (err) {
        throw new Error(err)
    }
}

export const updateCategoryByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const category: ICategory | null = await updateCategory(id, req.body)
        res.status(RES_STATUS.OK).json({ category })
    } catch (err) {
        throw new Error(err)
    }
}

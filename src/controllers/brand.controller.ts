import { Response, Request } from 'express'
import { IBrand } from 'src/const/commonType.const'
import { RES_STATUS } from 'src/const/responseStatus.const'
import {
    getAllBrands,
    getBrand,
    getBrandByName,
    updateBrand,
    deleteBrand,
    createBrand,
} from '../services/brand.services'
import { validateMongoID } from '../helpers/validate.helper'

const getBrands = async (_req: Request, res: Response) => {
    try {
        const brand: IBrand[] = await getAllBrands()

        res.status(RES_STATUS.OK).json({ brand })
    } catch (err) {
        throw new Error(err)
    }
}

const getBrandByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)

    try {
        const brand: IBrand | null = await getBrand(id)
        res.status(RES_STATUS.OK).json({ brand })
    } catch (err) {
        throw new Error(err)
    }
}

const deleteCategoryByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const brand: IBrand | null = await deleteBrand(id)
        res.status(RES_STATUS.OK).json({ brand })
    } catch (err) {
        throw new Error(err)
    }
}

const addCategory = async (req: Request, res: Response) => {
    try {
        const body = req.body
        if (!body) {
            throw new Error(
                'Body data request is undefined. Please check your request.'
            )
        }

        const isExistBrand: IBrand | null = await getBrandByName(body.name)
        if (isExistBrand) {
            throw new Error(
                `Brand ${body.name} already exists, Please change your billboard's name.`
            )
        }

        const newBrand: IBrand = await createBrand(body)

        res.status(RES_STATUS.CREATED).json(newBrand)
    } catch (err) {
        throw new Error(err)
    }
}

const updateCategoryByID = async (req: Request, res: Response) => {
    const { id } = req.params
    validateMongoID(id)
    try {
        const brand: IBrand | null = await updateBrand(id, req.body)
        res.status(RES_STATUS.OK).json({ brand })
    } catch (err) {
        throw new Error(err)
    }
}

export {
    getBrands,
    getBrandByID,
    deleteCategoryByID,
    updateCategoryByID,
    addCategory,
}

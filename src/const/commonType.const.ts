import { Document } from 'mongoose'

export interface IProduct {
    id: string
    title: string
    slug: string
    description: string
    category: ICategory
    brand: IBrand
    price: number
    quantity: number
    sold: number
    tags: string
    // size: Size
    color: string[]
    images: Image[]
}

export interface IBrand extends Document {
    id: String
    name: String
}

export interface IUser extends Document {
    id: String
    firstname: String
    lastname: String
    email: String
    mobile: String
    password: String
    role: String
    isBlocked: Boolean
    // cart: {
    //     type: Array,
    //     default: [],
    // },
    address: String
    // wishlist: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
    refreshToken: String
    passwordChangedAt: Date
    passwordResetToken: String
    passwordResetExpires: Date
}

export interface IOrder extends Document {
    id: String
    products: IProduct[]
    paymentIntent: {}
    orderStatus: String
    orderby: IUser
}

export interface ICart extends Document {
    id: String
    products: IProduct[]
    cartTotal: Number
    totalAfterDiscount: Number
    orderby: IUser
}

export interface Image {
    url: string
    public_id: string
}

export interface IBillboard {
    id: string
    label: string
    imageUrl?: string
    createAt?: String
    updateAt?: String
}

export interface ICategory {
    id: string
    name: string
    billboard: IBillboard
    createAt?: String
    updateAt?: String
}

export interface Size {
    id: string
    name: string
    value: string
}

export interface Color {
    id: string
    name: string
    value: string
}

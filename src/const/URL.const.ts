import dotenv from 'dotenv'

dotenv.config()

export const URI: string = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.r6t762t.mongodb.net/?retryWrites=true&w=majority`
